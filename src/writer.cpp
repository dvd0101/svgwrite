#include <fmt/format.h>
#include <ostream>
#include <svgwrite/writer.hpp>

namespace fmt {
	/**
	 * `fmt::formatter` specialization to format svgw::attr_list.
	 *
	 * @private
	 */
	template <>
	struct formatter<svgw::attr_list> {
		constexpr auto parse(format_parse_context& ctx) {
			auto it = ctx.begin(), end = ctx.end();
			if (it != end && *it != '}') {
				throw format_error("invalid format");
			}
			return it;
		}

		template <typename FormatContext>
		auto format(const svgw::attr_list& attrs, FormatContext& ctx) {
			for (auto&& attr : attrs.attrs) {
				std::visit([&](const auto& v) { return format_to(ctx.out(), R"""({}="{}" )""", attr.name, v); },
				           attr.value);
			}
			return ctx.out();
		}
	};

	/**
	 * `fmt::formatter` specialization to format svgw::attr_value.
	 *
	 * @private
	 */
	template <>
	struct formatter<svgw::attr_value> {
		constexpr auto parse(format_parse_context& ctx) {
			auto it = ctx.begin(), end = ctx.end();
			if (it != end && *it != '}') {
				throw format_error("invalid format");
			}
			return it;
		}

		template <typename FormatContext>
		auto format(const svgw::attr_value& l, FormatContext& ctx) {
			return std::visit([&](auto v) { return format_to(ctx.out(), "{}", v); }, l);
		}
	};
}

namespace svgw::v1 {

	namespace {
		template <typename... Args>
		void print(std::ostream& os, std::vector<char>& buffer, std::string_view format_str, Args&&... args) {
			buffer.clear();
			fmt::format_to(std::back_inserter(buffer), format_str, args...);
			buffer.push_back(0);
			os << buffer.data();
		}
	}

	void writer::start_svg(length width, length height, attr_list attrs) {
		print(
		    *os,
		    buffer,
		    R"""(<svg width="{}" height="{}" {} xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">)""",
		    width,
		    height,
		    attrs);
	}

	void writer::end_svg() { print(*os, buffer, "</svg>"); }

	void writer::start_g(attr_list attrs) { print(*os, buffer, "<g {}>", attrs); }

	void writer::end_g() { print(*os, buffer, "</g>"); }

	void writer::start_defs() { print(*os, buffer, "<defs>"); }

	void writer::end_defs() { print(*os, buffer, "</defs>"); }

	void writer::write(std::string_view frag) { *os << frag; }

	void writer::path(std::string_view path_data, attr_list attrs) {
		print(*os, buffer, R"""(<path d="{}" {}/>)""", path_data, attrs);
	}

	void writer::rect(coord x, coord y, length width, length height, attr_list attrs) {
		print(*os, buffer, R"""(<rect x="{}" y="{}" width="{}" height="{}" {}/>)""", x, y, width, height, attrs);
	}

	void writer::line(coord x1, coord y1, coord x2, coord y2, attr_list attrs) {
		print(*os, buffer, R"""(<line x1="{}" y1="{}" x2="{}" y2="{}" {}/>)""", x1, y1, x2, y2, attrs);
	}

	void writer::start_pattern(std::string_view id, attr_list attrs) {
		print(*os, buffer, R"""(<pattern id="{}" {}>)""", id, attrs);
	}

	void writer::end_pattern() { print(*os, buffer, "</pattern>"); }

	void writer::start_symbol(std::string_view id, attr_list attrs) {
		print(*os, buffer, R"""(<symbol id="{}" {}>)""", id, attrs);
	}

	void writer::end_symbol() { print(*os, buffer, "</symbol>"); }

	void writer::use(std::string_view href, attr_list attrs) {
		print(*os, buffer, R"""(<use xlink:href="{}" {}/>)""", href, attrs);
	}

	void writer::text(coord x, coord y, std::string_view text, attr_list attrs) {
		print(*os, buffer, R"""(<text x="{}" y="{}" {}>{}</text>)""", x, y, attrs, text);
	}
}
