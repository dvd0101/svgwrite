find_package(fmt REQUIRED)
find_package(span-lite REQUIRED)

file(GLOB_RECURSE sources CONFIGURE_DEPENDS *.cpp *.hpp)
file(GLOB_RECURSE public_headers CONFIGURE_DEPENDS ../include/svgwrite/*.hpp)

add_library(svgwrite ${sources} ${public_headers})
add_library(svgwrite::svgwrite ALIAS svgwrite)

set_target_properties(svgwrite
    PROPERTIES
        CXX_EXTENSIONS OFF
        PUBLIC_HEADER ${public_headers}
)

target_compile_features(svgwrite PUBLIC cxx_std_17)
target_compile_options(svgwrite
    PRIVATE
        $<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-Wall -Wextra -Wpedantic>
)
target_include_directories(svgwrite
    PUBLIC
	    $<BUILD_INTERFACE:${svgwrite_SOURCE_DIR}/include>
	    # $<INSTALL_INTERFACE:include>
    PRIVATE .
)
target_link_libraries(svgwrite
    PUBLIC nonstd::span-lite
    PRIVATE fmt::fmt
)
