from cpt.packager import ConanMultiPackager


if __name__ == "__main__":
    builder = ConanMultiPackager(cppstds=["17"], build_policy="missing")
    builder.add_common_builds(pure_c=False)

    def filter(b):
        # It looks like the conan docker image with clang does not have the
        # required library to link against 32bit libc++
        if b.settings["arch"] == "x86":
            return True
        # Save some time on the CI by not compiling the old libstdc++ ABI when
        # on GCC.
        # For clang "libstdc++" is compatible with the new libstdc++11 ABI.
        return b.settings["compiler"] == "gcc" and b.settings["compiler.libcxx"] == "libstdc++"
    builder.remove_build_if(filter)
    builder.run()
