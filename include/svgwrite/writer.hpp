#pragma once
#include <iosfwd>
#include <nonstd/span.hpp>
#include <string_view>
#include <variant>
#include <vector>

/**
 * The svgw namespace
 */
namespace svgw {
	inline namespace v1 {
		/**
		 * A type for the attributes values.
		 *
		 * An attribute can be:
		 * - an integral (std::int32_t, std::uint32_t, std::int64_t, std::uint64_t)
		 * - a floating point (double)
		 * - a string (std::string_view)
		 *
		 * Since the attributes are never stored it's safe to use a string_view for the string values.
		 *
		 * There are four different types for the integral values to reduce to the minimum the need for an explicit cast
		 * at the caller site.
		 */
		using attr_value =
		    std::variant<std::int32_t, std::uint32_t, std::int64_t, std::uint64_t, double, std::string_view>;

		/**
		 * coord is a [svg coordinate](https://www.w3.org/TR/SVG11/types.html#DataTypeCoordinate).
		 *
		 * A coord can be a number (integer or floating point) or a string (eg. "12cm").
		 */
		using coord = attr_value;

		/**
		 * length is a [svg length](https://www.w3.org/TR/SVG11/types.html#DataTypeLength).
		 *
		 * A length can be a number (integer or floating point) or a string (eg. "12cm").
		 */
		using length = attr_value;

		/**
		 * A tag attribute.
		 *
		 * An attrbute has a name and a value, the value can be a number (int or double) or a string.
		 */
		struct attr {
			std::string_view name; ///< The attribute name.
			attr_value value;      ///< The attribute value.
		};

		/**
		 * A sequence of tag attributes (svgw::attr).
		 *
		 */
		class attr_list {
		  public:
			/**
			 * Construct a new attr_list from a `std::initializer_list<>`.
			 *
			 * Example:
			 *
			 * ```
			 * writer.start_svg("12cm", "4cm", {{"viewBox", "0 0 1200 400"}});
			 * ```
			 */
			attr_list(std::initializer_list<attr> a) : attrs{a.begin(), a.end()} {}

			/**
			 * Construct a new attr_list from any type that can be converted to a `span<const attr>`.
			 *
			 * A `span<const attr>` is a contiguous sequence of `const svgw::attr`.
			 *
			 * Example:
			 *
			 * ```
			 * std::vector<svgw::attr> attrs = {{"stroke-width", 10}};
			 * if (condition) {
			 *     attrs.push_back({"stroke", "red"});
			 * }
			 * writer.line(300, 300, 500, 100, attrs);
			 * ```
			 */
			template <typename T,
			          typename SFINAE = std::enable_if_t<std::is_constructible_v<nonstd::span<const attr>, T>>>
			attr_list(T x) : attrs{x} {}

		  public:
			nonstd::span<const attr> attrs;
		};

		/**
		 * A `writer` class to generate a svg document and writes it to an output stream.
		 *
		 * A `writer` instance has an private vector<char> used by all the methods as a temporary buffer to format
		 * the various xml fragments.
		 *
		 * By reusing the same buffer we kept the memory allocations to the minimum.
		 */
		class writer {
		  public:
			/** Construct a new writer object to write on `os` */
			writer(std::ostream& os) : os{&os} {}
			/** A writer is not copyable (since it has a reference to the output stream) */
			writer(const writer&) = delete;
			/** A writer can be moved **but be careful** the moved from instance cannot be used anymore */
			writer(writer&& other) : buffer{std::move(other.buffer)}, os{other.os} { other.os = nullptr; }

			/** A writer is not copy assignable */
			writer& operator=(const writer&) = delete;
			/** A writer is move assigable **but be careful** the moved from instance cannot be used anymore */
			writer& operator=(writer&& other) {
				buffer = std::move(other.buffer);
				os = other.os;
				other.os = nullptr;
				return *this;
			}

		  public:
			/**
			 * Opens the `<svg>` tag.
			 *
			 * ### Examples
			 *
			 * - `start_svg(200, 100);`
			 *
			 *      starts an svg with a size of 200x100 pixels (to be precise a [number without
			 *      unit](https://www.w3.org/TR/SVG11/types.html#DataTypeLength) refers to the current user coordinate
			 *      system, but usually it means "pixels").
			 *
			 * - `start_svg("12cm", "4cm");`
			 *
			 *      starts an svg with a size of 12x4 cm.
			 *
			 * - `start_svg("12cm", "4cm", {{"viewBox", "0 0 1200 400"}});`
			 *
			 *      starts an svg with a size of 12x4 cm and a viewBox of 1200x400 pixels.
			 *
			 * The following XML attributes are automatically added:
			 *
			 * attribute | value
			 * --------- | -----
			 * xmlns | http://www.w3.org/2000/svg
			 * version | 1.1
			 *
			 * @see http://www.w3.org/TR/SVG11/struct.html#SVGElement
			 */
			void start_svg(length width, length height, attr_list attrs = {});
			/** Closes the `<svg>` tag. */
			void end_svg();

			/**
			 * Opens the `<g>` tag.
			 *
			 * @see http://www.w3.org/TR/SVG11/struct.html#GElement
			 */
			void start_g(attr_list attrs = {});
			/** Closes the `<g>` tag. */
			void end_g();

			/**
			 * Opens the `<defs>` tag.
			 *
			 * The defs element is a container element for referenced elements. For understandability and accessibility
			 * reasons, it is recommended that, whenever possible, referenced elements be defined inside of a defs.
			 *
			 * @see http://www.w3.org/TR/SVG11/struct.html#DefsElement
			 */
			void start_defs();
			/** Closes the `<defs>` tag. */
			void end_defs();

			/** Writes a svg fragment on the stream as is. */
			void write(std::string_view frag);

		  public:
			/**
			 * Writes a `<path>` tag.
			 *
			 * The path element represent the outline of a shape which can be filled, stroked, used as a clipping path,
			 * or any combination of the three.
			 *
			 * @see http://www.w3.org/TR/SVG11/paths.html#PathElement
			 */
			void path(std::string_view path_data, attr_list attrs = {});

			/**
			 * Writes a `<line>` tag.
			 *
			 * The line element defines a line segment that starts at one point and ends at another.
			 *
			 * @see http://www.w3.org/TR/SVG11/shapes.html#LineElement
			 */
			void line(coord x1, coord y1, coord x2, coord y2, attr_list attrs = {});

			/**
			 * Writes a `<rect>` tag.
			 *
			 * The rect element defines a rectangle which is axis-aligned with the current user coordinate system.
			 *
			 * @see http://www.w3.org/TR/SVG11/shapes.html#RectElement
			 */
			void rect(coord x, coord y, length width, length height, attr_list attrs = {});

		  public:
			/**
			 * Opens the `<pattern>` tag.
			 *
			 * A pattern is used to fill or stroke an object using a pre-defined graphic object which can be replicated
			 * (“tiled”) at fixed intervals in x and y to cover the areas to be painted. Patterns are defined using a
			 * pattern element and then referenced by properties fill and stroke on a given graphics element to indicate
			 * that the given element shall be filled or stroked with the referenced pattern.
			 *
			 * @see http://www.w3.org/TR/SVG11/pservers.html#PatternElement
			 */
			void start_pattern(std::string_view id, attr_list attrs = {});
			/** Closes the `<pattern>` tag. */
			void end_pattern();

			/**
			 * Opens the `<symbol>` tag.
			 *
			 * The symbol element is used to define graphical template objects which can be instantiated by a use
			 * element.
			 *
			 * @see http://www.w3.org/TR/SVG11/struct.html#SymbolElement
			 */
			void start_symbol(std::string_view id, attr_list attrs = {});
			/** Closes the `<symbol>` tag. */
			void end_symbol();

			/**
			 * Writes a `<use>` tag.
			 *
			 * The use element references another element and indicates that the graphical contents of that element is
			 * included/drawn at that given point in the document.
			 *
			 * @see http://www.w3.org/TR/SVG11/struct.html#UseElement
			 */
			void use(std::string_view href, attr_list attrs = {});

		  public:
			/**
			 * Writes a `<text>` tag.
			 *
			 * Text that is to be rendered as part of an SVG document fragment is specified using the text element.
			 *
			 * @see http://www.w3.org/TR/SVG11/text.html#TextElement
			 */
			void text(coord x, coord y, std::string_view text, attr_list attrs = {});

		  private:
			std::vector<char> buffer;
			std::ostream* os;
		};
	}
}