# SVGWrite - a streamign svg library {#index}

**svgwrite** is an open-source c++17 library for streaming SVG documents.

It does not construct an in memory DOM nor buffers the output; it writes the SVG fragments to the output stream whenever they are produced.

svgwrite is inspired by the go [svgo](https://github.com/ajstarks/svgo) library with which it shares some design decisions.

## What it looks like

```cpp
#include <iostream>
#include <svgwrite/writer.hpp>

int main() {
	svgw::writer w(std::cout);

	w.start_svg("12cm", "4cm", {{"viewBox", "0 0 1200 400"}});
	w.rect(1, 1, 1198, 398, {{"fill", "none"}, {"stroke", "blue"}, {"stroke-width", "2"}});

	w.start_g({{"stroke", "green"}});
	w.line(100, 300, 300, 100, {{"stroke-width", 5}});
	w.line(300, 300, 500, 100, {{"stroke-width", 10}});
	w.line(500, 300, 700, 100, {{"stroke-width", 15}});
	w.line(700, 300, 900, 100, {{"stroke-width", 20}});
	w.line(900, 300, 1100, 100, {{"stroke-width", 25.2}});
	w.end_g();

	w.end_svg();
}
```

An *svgw::writer* instance is created by providing an *ostream* where to write.

Each method of the **writer** is associated with a corresponding SVG tag (or half tag as in the case of `<g>`), which is immediately written to the *ostream*.

Memory allocations are kept to the minimum by using a per-writer buffer where XML fragments are constructed.

The above code produces this SVG:

```xml
<?xml version="1.0"?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12cm" height="4cm" viewBox="0 0 1200 400" version="1.1">
  <rect x="1" y="1" width="1198" height="398" fill="none" stroke="blue" stroke-width="2"/>
  <g stroke="green">
    <line x1="100" y1="300" x2="300" y2="100" stroke-width="5"/>
    <line x1="300" y1="300" x2="500" y2="100" stroke-width="10"/>
    <line x1="500" y1="300" x2="700" y2="100" stroke-width="15"/>
    <line x1="700" y1="300" x2="900" y2="100" stroke-width="20"/>
    <line x1="900" y1="300" x2="1100" y2="100" stroke-width="25.2"/>
  </g>
</svg>
```

## How to get it

svgwrite is available in the [conan center index](https://conan.io/center/) or you can use CMake to build and use it.

svgwrite itself depends on:

- [fmt](https://fmt.dev/latest/index.html) (This is a private dependency need to build svgwrite, not to use it)
- [span lite](https://github.com/martinmoene/span-lite)

### conan

```
    conan install svgwrite/0.2.0 --build=missing
```

svgwrite is a c++17 library, if your default profile use a lower standard you need to add `-s compiler.cppstd=17` to the above command.

This will install svgwrite and its dependencies.

### cmake

svgwrite provides first-class support for CMake (if you encounter any problem please open an issue), you can:

- build the library, install and find it (`find_package`)
- add the library to your CMakeLists.txt (`add_subdirectory`)

The exported target is **svgwrite::svgwrite**

You need to provide the required dependencies.

# Library details

## Custom attributes

The writer's methods signatures only require the mandatory attributes (as by the SVG specs) and resort to an
`svgw::attr_list` for everything else.

An `attr_list` is a sequence of `svgw::attr`, a name/value pair, stored as a `std::initializer_list` or a
`std::vector`.

```cpp
    w.line(100, 300, 300, 100, {{"stroke-width", 5}}); // An attr_list is created from a std::initializer_list

    std::vector<svgw::attr> attrs = {{"stroke-width", 10}};
    if (condition) {
        attrs.push_back({"stroke", "red"});
    }
    w.line(300, 300, 500, 100, attrs);
```

## Coordinates and lengths

[Coordinates](https://www.w3.org/TR/SVG11/types.html#DataTypeCoordinate) and
[lengths](https://www.w3.org/TR/SVG11/types.html#DataTypeLength) are implemented as a:

    std::variant<std::int32_t, std::uint32_t, std::int64_t, std::uint64_t, double, std::string_view>

By including the `string_view` type we support values like *12cm*.

The four different integral types are present to minimize the need of an explicit cast at the caller site.

Integral types are not strictly required, according to the [spec](https://www.w3.org/TR/SVG11/types.html#DataTypeNumber) an svg number can be represented by a *double*, but since integral values are common it make sense to kept them and leverage a faster conversion to string.
