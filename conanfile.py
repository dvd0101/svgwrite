from conans import ConanFile, CMake, tools


class SvgwriteConan(ConanFile):
    name = "svgwrite"
    version = "0.2.0"

    license = "Boost Software License"
    author = "dvd0101 <dvd0101@gnx.it>"
    url = "https://gitlab.com/dvd0101/svgwrite"
    description = "SVGWrite - a streamign svg library"
    topics = ("c++", "svg")

    requires = "span-lite/0.7.0", "fmt/6.1.2"

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {
        "shared": False,
    }

    generators = "cmake", "cmake_paths", "cmake_find_package"
    exports_sources = (
        "LICENSE",
        "CMakeLists.txt",
        "svgwriteConfig.cmake",
        "src/*",
        "include/*",
        "example/*",
    )

    def _cmake(self):
        if getattr(self, "_cache_cmake", None) is None:
            tools.replace_in_file(
                "CMakeLists.txt",
                "add_subdirectory(src)",
                """
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
add_subdirectory(src)
"""
            )
            cmake = CMake(self)
            cmake.configure()
            self._cache_cmake = cmake
        return self._cache_cmake

    def configure(self):
        tools.check_min_cppstd(self, "17")

    def build(self):
        cmake = self._cmake()
        cmake.build()

    def package(self):
        cmake = self._cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["svgwrite"]
